# README #

Built using Java 8, Gradle, Guava and Spring and Mockito for testing.

To build and run hipsterchat:
```
gradle build
java -jar build/libs/hipsterchat.jar
```
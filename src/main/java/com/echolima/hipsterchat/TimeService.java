package com.echolima.hipsterchat;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

/**
 * Wraps date/time creation.
 */
@Service
public class TimeService {
  public LocalDateTime now() {
    return LocalDateTime.now();
  }
}

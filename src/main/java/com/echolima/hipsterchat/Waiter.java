package com.echolima.hipsterchat;

import java.util.List;

import com.echolima.hipsterchat.commands.HipsterCommand;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Repository;

/**
 * In memory storage for commands.
 */
@Repository
public class Waiter {
  private List<HipsterCommand> commandHistory = Lists.newArrayList();

  public void addCommand(HipsterCommand command) {
    commandHistory.add(command);
  }

  public ImmutableList<HipsterCommand> getCommandHistory() {
    return ImmutableList.copyOf(commandHistory);
  }
}

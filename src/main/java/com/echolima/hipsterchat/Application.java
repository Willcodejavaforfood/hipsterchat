package com.echolima.hipsterchat;

import java.io.Console;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Entry point to start the app and contains the configuration for beans.
 *
 * Please note that to use the console the app has to be started from the jar using
 * java -jar hipsterchat.jar
 */
@ComponentScan("com.echolima.hipsterchat")
@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(Application.class);
    app.setWebEnvironment(false);
    app.run(args);
  }

  @Bean
  public Console getSystemConsole() {
    return System.console();
  }
}

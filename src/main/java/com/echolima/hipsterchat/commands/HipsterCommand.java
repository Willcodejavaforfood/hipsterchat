package com.echolima.hipsterchat.commands;

import com.echolima.hipsterchat.HipsterVisitor;

/**
 * All commands by users will implement this so {@code TheCafe} can execute them.
 */
public interface HipsterCommand {
  void accept(HipsterVisitor hipster);
}

package com.echolima.hipsterchat.commands;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.HipsterVisitor;

/**
 * The posion pill, or someone ordered a venti latte, which terminates the chat.
 */
public class GoodbyeCommand implements HipsterCommand {
  private final LocalDateTime date;

  public GoodbyeCommand(LocalDateTime date) {
    this.date = date;
  }

  public LocalDateTime getDate() {
    return date;
  }

  @Override
  public void accept(HipsterVisitor hipster) {
    hipster.visit(this);
  }
}

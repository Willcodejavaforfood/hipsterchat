package com.echolima.hipsterchat.commands;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.HipsterVisitor;

/**
 * Saves a chat by a user to their timeline.
 */
public final class SendChatCommand implements HipsterCommand {
  private final String message;
  private final String user;
  private final LocalDateTime date;

  public SendChatCommand(String user, String message, LocalDateTime date) {
    this.user = user;
    this.message = message;
    this.date = date;
  }

  public String getMessage() {
    return message;
  }

  public String getUser() {
    return user;
  }

  public LocalDateTime getDate() {
    return date;
  }

  @Override
  public void accept(HipsterVisitor hipster) {
    hipster.visit(this);
  }
}

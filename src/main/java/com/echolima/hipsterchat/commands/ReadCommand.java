package com.echolima.hipsterchat.commands;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.HipsterVisitor;

/**
 * Command for a user to view the timeline/wall of another user.
 */
public class ReadCommand implements HipsterCommand {
  private String user;
  private LocalDateTime date;

  public ReadCommand(String user, LocalDateTime date) {
    this.user = user;
    this.date = date;
  }

  public String getUser() {
    return user;
  }

  public LocalDateTime getDate() {
    return date;
  }

  @Override
  public void accept(HipsterVisitor hipster) {
    hipster.visit(this);
  }
}

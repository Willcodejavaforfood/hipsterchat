package com.echolima.hipsterchat.commands;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.HipsterVisitor;

/**
 * Command for a user to follow another user. This will add the messages of that user to their
 * wall or timeline.
 */
public class FollowCommand implements HipsterCommand {
  private final String user;
  private final String following;
  private final LocalDateTime date;

  public FollowCommand(String user, String following, LocalDateTime date) {
    this.user = user;
    this.following = following;
    this.date = date;
  }

  public String getUser() {
    return user;
  }

  public String getFollowing() {
    return following;
  }

  public LocalDateTime getDate() {
    return date;
  }

  @Override
  public void accept(HipsterVisitor hipster) {
    hipster.visit(this);
  }
}

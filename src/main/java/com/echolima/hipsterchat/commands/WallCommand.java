package com.echolima.hipsterchat.commands;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.HipsterVisitor;

/**
 * Command for a user to view their wall of messages.
 */
public class WallCommand implements HipsterCommand {
  private final String user;
  private final LocalDateTime date;

  public WallCommand(String user, LocalDateTime date) {
    this.user = user;
    this.date = date;
  }

  public String getUser() {
    return user;
  }

  public LocalDateTime getDate() {
    return date;
  }

  @Override
  public void accept(HipsterVisitor hipster) {
    hipster.visit(this);
  }
}

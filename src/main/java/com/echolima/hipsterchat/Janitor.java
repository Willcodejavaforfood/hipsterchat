package com.echolima.hipsterchat;

import org.springframework.stereotype.Component;

/**
 * Responsible for closing and cleaning up the cafe.
 */
@Component
public class Janitor {
  public void shutdown() {
    System.exit(0);
  }
}

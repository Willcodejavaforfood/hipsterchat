package com.echolima.hipsterchat;

import com.echolima.hipsterchat.commands.FollowCommand;
import com.echolima.hipsterchat.commands.GoodbyeCommand;
import com.echolima.hipsterchat.commands.ReadCommand;
import com.echolima.hipsterchat.commands.SendChatCommand;
import com.echolima.hipsterchat.commands.WallCommand;

/**
 * Visitor design pattern.
 */
public interface HipsterVisitor {
  void visit(SendChatCommand chatCommand);
  void visit(WallCommand wallCommand);
  void visit(FollowCommand followCommand);
  void visit(ReadCommand readCommand);
  void visit(GoodbyeCommand goodbyeCommand);
}

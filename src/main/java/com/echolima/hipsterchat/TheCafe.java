package com.echolima.hipsterchat;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

import com.echolima.hipsterchat.commands.FollowCommand;
import com.echolima.hipsterchat.commands.GoodbyeCommand;
import com.echolima.hipsterchat.commands.HipsterCommand;
import com.echolima.hipsterchat.commands.ReadCommand;
import com.echolima.hipsterchat.commands.SendChatCommand;
import com.echolima.hipsterchat.commands.WallCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Controller;

/**
 * A gathering place for hipsters so they can chat with each other and compare beards.
 */
@Controller
public class TheCafe implements CommandLineRunner, HipsterVisitor {
  private static final String BANNER_1 = "        _           _                _           _      \n";
  private static final String BANNER_2 = "  /\\  /(_)_ __  ___| |_ ___ _ __ ___| |__   __ _| |_   \n";
  private static final String BANNER_3 = " / /_/ / | '_ \\/ __| __/ _ \\ '__/ __| '_ \\ / _` | __|\n";
  private static final String BANNER_4 = "/ __  /| | |_) \\__ \\ ||  __/ | | (__| | | | (_| | |_ \n";
  private static final String BANNER_5 = "\\/ /_/ |_| .__/|___/\\__\\___|_|  \\___|_| |_|\\__,_|\\__|\n";
  private static final String BANNER_6 = "         |_|                                         \n";
  private static final String MESSAGE_WELCOME = "Welcome to HipsterChat, the home for bearded geeks.\n";

  private final HipsterChat chat;
  private final TheBarrista theBarrista;
  private final Waiter waiter;
  private final TimeService timeService;
  private Janitor janitor;
  private boolean cafeOpen = true;

  @Autowired
  public TheCafe(HipsterChat chat, TheBarrista theBarrista, Waiter waiter, TimeService timeService, Janitor janitor) {
    this.chat = chat;
    this.theBarrista = theBarrista;
    this.waiter = waiter;
    this.timeService = timeService;
    this.janitor = janitor;
  }

  @Override
  public void run(String... args) throws Exception {
    start();
    startAcceptingInput();
  }

  public void startAcceptingInput() {
    do {
      HipsterCommand hipsterCommand = theBarrista.parse(chat.nextCommand().orElse(""));
      hipsterCommand.accept(this);
    } while (cafeOpen);
  }

  public void start() {
    chat.writeToTerminal(BANNER_1);
    chat.writeToTerminal(BANNER_2);
    chat.writeToTerminal(BANNER_3);
    chat.writeToTerminal(BANNER_4);
    chat.writeToTerminal(BANNER_5);
    chat.writeToTerminal(BANNER_6);
    chat.writeToTerminal("");
    chat.writeToTerminal(MESSAGE_WELCOME);
  }

  @Override
  public void visit(SendChatCommand chatCommand) {
    waiter.addCommand(chatCommand);
  }

  @Override
  public void visit(WallCommand wallCommand) {
    waiter.addCommand(wallCommand);

    List<String> following = waiter.getCommandHistory().stream()
        .filter(c -> c instanceof FollowCommand)
        .map(c -> (FollowCommand) c)
        .filter(c -> c.getUser().equals(wallCommand.getUser()))
        .map(FollowCommand::getFollowing)
        .collect(Collectors.toList());
    following.add(wallCommand.getUser()); // Don't forget to add ourselves to the list or we wont see our own messages.

    waiter.getCommandHistory().reverse().stream()
        .filter(c -> c instanceof SendChatCommand)
        .map(c -> (SendChatCommand) c)
        .filter(c -> following.contains(c.getUser()))
        .forEach(c -> printWallMessage(c));
  }

  @Override
  public void visit(FollowCommand followCommand) {
    waiter.addCommand(followCommand);
  }

  @Override
  public void visit(final ReadCommand readCommand) {
    waiter.addCommand(readCommand);

    waiter.getCommandHistory().reverse().stream()
        .filter(c -> c instanceof SendChatCommand)
        .map(c -> (SendChatCommand) c)
        .filter(c -> c.getUser().equals(readCommand.getUser()))
        .forEach(c -> printReadMessage(c));
  }

  @Override
  public void visit(GoodbyeCommand goodbyeCommand) {
    waiter.addCommand(goodbyeCommand);
    cafeOpen = false;
    chat.writeToTerminal("Goodbye!");
    janitor.shutdown();
  }

  private void printWallMessage(SendChatCommand command) {
    chat.writeToTerminal(
        String.format(
            "%s -> %s (%s)\n",
            command.getUser(),
            command.getMessage(),
            timestamp(command.getDate(), timeService.now())));
  }

  private void printReadMessage(SendChatCommand command) {
    chat.writeToTerminal(
        String.format(
            "%s (%s)\n",
            command.getMessage(),
            timestamp(command.getDate(), timeService.now())));
  }

  private static String timestamp(LocalDateTime from, LocalDateTime now) {
    long delta = ChronoUnit.SECONDS.between(from, now);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(delta < 60 ? delta : delta / 60);
    stringBuilder.append(" ");
    stringBuilder.append(delta < 60 ? "seconds" : "minutes");
    stringBuilder.append(" ago");

    return stringBuilder.toString();
  }
}

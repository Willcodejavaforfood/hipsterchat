package com.echolima.hipsterchat;

import java.io.Console;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Wrapper around {@code Console} to allow mocking and a simplified API for interacting with
 * the terminal.
 */
@Component
public class HipsterChat {
  private static final String MESSAGE_PROMPT = "> ";

  private final Console console;

  @Autowired
  public HipsterChat(Console console) {
    this.console = console;
  }

  /**
   * @return The next line of user input or Optional.empty() if none is given.
   */
  public Optional<String> nextCommand() {
    return Optional.ofNullable(console.readLine(MESSAGE_PROMPT));
  }

  /**
   * @param message Appends message to the terminal.
   */
  public void writeToTerminal(String message) {
    console.printf(message);
  }
}

package com.echolima.hipsterchat.parsers;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.HipsterCommand;
import com.echolima.hipsterchat.commands.ReadCommand;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Parses {@code ReadCommand} instances.
 */
@Component
public class ReadParser implements CommandParser {
  private static final int INDEX_USER = 0;

  private final TimeService timeService;

  @Autowired
  public ReadParser(TimeService timeService) {
    this.timeService = timeService;
  }

  @Override
  public boolean canHandle(String userInput) {
    return !Strings.isNullOrEmpty(userInput)
        && userInput.split(" ").length == 1
        && !userInput.equals("Exit");
  }

  @Override
  public HipsterCommand parse(String userInput) {
    return new ReadCommand(userInput.trim(), timeService.now());
  }
}

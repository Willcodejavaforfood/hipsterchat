package com.echolima.hipsterchat.parsers;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.FollowCommand;
import com.echolima.hipsterchat.commands.HipsterCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Parses instances of {@code FollowCommand}.
 */
@Component
public class FollowParser implements CommandParser {
  private static final int INDEX_USER = 0;
  private static final int INDEX_FOLLOWING = 1;

  private final TimeService timeService;

  @Autowired
  public FollowParser(TimeService timeService) {
    this.timeService = timeService;
  }

  @Override public boolean canHandle(String userInput) {
    return userInput.contains("follows");
  }

  @Override public HipsterCommand parse(String userInput) {
    String[] commandParts = userInput.split("follows");
    return new FollowCommand(
        commandParts[INDEX_USER].trim(), commandParts[INDEX_FOLLOWING].trim(), timeService.now());
  }
}

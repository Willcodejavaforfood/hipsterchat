package com.echolima.hipsterchat.parsers;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.GoodbyeCommand;
import com.echolima.hipsterchat.commands.HipsterCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * For when it's time to say goodbye.
 */
@Component
public class GoodbyeParser implements CommandParser {
  private final TimeService timeService;

  @Autowired
  public GoodbyeParser(TimeService timeService) {
    this.timeService = timeService;
  }

  @Override
  public boolean canHandle(String userInput) {
    return userInput.equals("Exit");
  }

  @Override
  public HipsterCommand parse(String userInput) {
    return new GoodbyeCommand(timeService.now());
  }
}

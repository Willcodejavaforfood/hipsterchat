package com.echolima.hipsterchat.parsers;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.HipsterCommand;
import com.echolima.hipsterchat.commands.SendChatCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SendChatParser implements CommandParser {
  private static final int INDEX_USER = 0;
  private static final int INDEX_MESSAGE = 1;

  private final TimeService timeService;

  @Autowired
  public SendChatParser(TimeService timeService) {
    this.timeService = timeService;
  }

  @Override
  public boolean canHandle(String userInput) {
    return userInput.split("->").length == 2;
  }

  @Override
  public HipsterCommand parse(String userInput) {
    String[] commandParts = userInput.split("->");
    return new SendChatCommand(
        commandParts[INDEX_USER].trim(),
        commandParts[INDEX_MESSAGE].trim(),
        timeService.now());
  }
}

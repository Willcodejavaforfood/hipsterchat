package com.echolima.hipsterchat.parsers;

import com.echolima.hipsterchat.commands.HipsterCommand;

/**
 * Command Parser contract.
 */
public interface CommandParser {
  boolean canHandle(String userInput);

  HipsterCommand parse(String userInput);
}

package com.echolima.hipsterchat.parsers;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.HipsterCommand;
import com.echolima.hipsterchat.commands.WallCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class WallParser implements CommandParser {
  private static final int INDEX_USER = 0;

  private final TimeService timeService;

  @Autowired
  public WallParser(TimeService timeService) {
    this.timeService = timeService;
  }

  @Override
  public boolean canHandle(String userInput) {
    return userInput.endsWith("wall");
  }

  @Override
  public HipsterCommand parse(String userInput) {
    String[] commandParts = userInput.split(" ");
    return new WallCommand(commandParts[INDEX_USER].trim(), timeService.now());
  }
}

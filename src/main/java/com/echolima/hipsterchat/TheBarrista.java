package com.echolima.hipsterchat;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import com.echolima.hipsterchat.commands.HipsterCommand;
import com.echolima.hipsterchat.parsers.CommandParser;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Parses user input into {@code HipsterCommand} instances.
 */
@Component
public class TheBarrista {
  private final Collection<CommandParser> parsers;

  @Autowired
  public TheBarrista(Collection<CommandParser> parsers) {
    this.parsers = parsers;
  }

  public HipsterCommand parse(String userInput) {
    Set<HipsterCommand> result = parsers.stream()
        .filter(parser -> parser.canHandle(userInput))
        .map(parser -> parser.parse(userInput))
        .collect(Collectors.toSet());

    if (result.size() > 0) {
      return Iterables.getOnlyElement(result);
    }
    throw new IllegalArgumentException("The Barrista cannot take your order");
  }
}

package com.echolima.hipsterchat;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Test;

import com.echolima.hipsterchat.commands.HipsterCommand;
import com.echolima.hipsterchat.parsers.CommandParser;
import com.echolima.hipsterchat.parsers.WallParser;
import com.google.common.collect.ImmutableList;

public class TheBarristaTest {
  @Test
  public void shouldFindAParserToParseWith() {
    ImmutableList<CommandParser> parsers = ImmutableList.of(new WallParser(new TimeService()));
    TheBarrista theBarrista = new TheBarrista(parsers);

    HipsterCommand hipsterCommand = theBarrista.parse("A wall");

    assertThat("No command was parsed", hipsterCommand, is(notNullValue()));
  }
}

package com.echolima.hipsterchat;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import com.echolima.hipsterchat.commands.FollowCommand;
import com.echolima.hipsterchat.commands.GoodbyeCommand;
import com.echolima.hipsterchat.commands.HipsterCommand;
import com.echolima.hipsterchat.commands.ReadCommand;
import com.echolima.hipsterchat.commands.SendChatCommand;
import com.echolima.hipsterchat.commands.WallCommand;
import com.google.common.collect.ImmutableList;

@RunWith(MockitoJUnitRunner.class)
public class TheCafeTest {
  public static final LocalDateTime TEST_DATE = LocalDateTime.of(2015, 1, 1, 10, 0, 0);
  public static final Optional<String> COMMAND_STRING_EXIT = Optional.of("Exit");
  @InjectMocks
  private TheCafe controller;
  @Mock
  private HipsterChat chat;
  @Mock
  private TheBarrista theBarrista;
  @Mock
  private Waiter waiter;
  @Mock
  private TimeService timeService;
  @Mock
  private Janitor janitor;

  @Before
  public void setup() {
    GoodbyeCommand goodbyeCommand = new GoodbyeCommand(TEST_DATE);
    given(theBarrista.parse(COMMAND_STRING_EXIT.get())).willReturn(goodbyeCommand);
  }

  @Test
  public void shouldSeeAWelcomeMessageWhenStartingHipsterChat() throws Exception {
    given(chat.nextCommand()).willReturn(COMMAND_STRING_EXIT);
    controller.run(new String[0]);
    verify(chat).writeToTerminal("Welcome to HipsterChat, the home for bearded geeks.\n");
  }

  @Test
  public void shouldSendAChatForNewUser() throws Exception {
    Optional<String> userInput = Optional.of("Erik -> Hello World!");
    given(chat.nextCommand()).willReturn(userInput, COMMAND_STRING_EXIT);
    given(theBarrista.parse(userInput.get()))
        .willReturn(new SendChatCommand("Erik", "Hello World!", TEST_DATE));
    controller.startAcceptingInput();
  }

  @Test
  public void shouldSeeMyPreviousMessagesUsingWall() {
    Optional<String> message1 = Optional.of("Erik -> Message 1");
    Optional<String> message2 = Optional.of("Erik -> Message 2");
    Optional<String> message3 = Optional.of("Erik -> Message 3");
    Optional<String> wall = Optional.of("Erik wall");

    given(chat.nextCommand()).willReturn(message1, message2, message3, wall, COMMAND_STRING_EXIT);
    HipsterCommand chatCommand1 =
        new SendChatCommand("Erik", "Message 1", TEST_DATE.minusMinutes(5));
    HipsterCommand chatCommand2 =
        new SendChatCommand("Erik", "Message 2", TEST_DATE.minusMinutes(1));
    HipsterCommand chatCommand3 =
        new SendChatCommand("Erik", "Message 3", TEST_DATE.minusSeconds(25));
    WallCommand wallCommand = new WallCommand("Erik", TEST_DATE.minusSeconds(5));
    given(theBarrista.parse(message1.get())).willReturn(chatCommand1);
    given(theBarrista.parse(message2.get())).willReturn(chatCommand2);
    given(theBarrista.parse(message3.get())).willReturn(chatCommand3);
    given(theBarrista.parse(wall.get())).willReturn(wallCommand);
    given(waiter.getCommandHistory())
        .willReturn(ImmutableList.of(chatCommand1, chatCommand2, chatCommand3, wallCommand));
    given(timeService.now()).willReturn(TEST_DATE);

    controller.startAcceptingInput();

    verify(waiter, times(5)).addCommand(any(HipsterCommand.class));
    verify(chat).writeToTerminal(String.format("%s (%s)\n", message3.get(), "25 seconds ago"));
    verify(chat).writeToTerminal(String.format("%s (%s)\n", message2.get(), "1 minutes ago"));
    verify(chat).writeToTerminal(String.format("%s (%s)\n", message1.get(), "5 minutes ago"));
  }

  @Test
  public void shouldSeeMessagesForOneUserUsingRead() {
    Optional<String> message1 = Optional.of("Erik -> Message 1");
    Optional<String> message2 = Optional.of("Irene -> Message 2");
    Optional<String> message3 = Optional.of("Sandra -> Message 3");
    Optional<String> read = Optional.of("Irene");

    given(chat.nextCommand()).willReturn(message1, message2, message3, read, COMMAND_STRING_EXIT);
    HipsterCommand chatCommand1 =
        new SendChatCommand("Erik", "Message 1", TEST_DATE.minusMinutes(5));
    HipsterCommand chatCommand2 =
        new SendChatCommand("Irene", "Message 2", TEST_DATE.minusMinutes(1));
    HipsterCommand chatCommand3 =
        new SendChatCommand("Sandra", "Message 3", TEST_DATE.minusSeconds(25));
    ReadCommand readCommand = new ReadCommand("Irene", TEST_DATE.minusSeconds(5));
    given(theBarrista.parse(message1.get())).willReturn(chatCommand1);
    given(theBarrista.parse(message2.get())).willReturn(chatCommand2);
    given(theBarrista.parse(message3.get())).willReturn(chatCommand3);
    given(theBarrista.parse(read.get())).willReturn(readCommand);
    given(waiter.getCommandHistory())
        .willReturn(ImmutableList.of(chatCommand1, chatCommand2, chatCommand3, readCommand));
    given(timeService.now()).willReturn(TEST_DATE);

    controller.startAcceptingInput();

    verify(waiter, times(5)).addCommand(any(HipsterCommand.class));
    verify(chat).writeToTerminal(String.format("%s (%s)\n", "Message 2", "1 minutes ago"));
  }

  @Test
  public void shouldSeeMessagesFromPeopleIFollowWhenILookAtMyWall() {
    Optional<String> message1 = Optional.of("Erik -> Message 1");
    Optional<String> message2 = Optional.of("Irene -> Message 2");
    Optional<String> message3 = Optional.of("Sandra -> Message 3");
    Optional<String> follow = Optional.of("Erik follows Irene");
    Optional<String> wall = Optional.of("Erik wall");

    given(chat.nextCommand()).willReturn(message1, message2, message3, follow, wall, COMMAND_STRING_EXIT);
    HipsterCommand chatCommand1 =
        new SendChatCommand("Erik", "Message 1", TEST_DATE.minusMinutes(5));
    HipsterCommand chatCommand2 =
        new SendChatCommand("Irene", "Message 2", TEST_DATE.minusMinutes(1));
    HipsterCommand chatCommand3 =
        new SendChatCommand("Sandra", "Message 3", TEST_DATE.minusSeconds(25));
    FollowCommand followCommand = new FollowCommand("Erik", "Irene", TEST_DATE.minusSeconds(5));
    WallCommand wallCommand = new WallCommand("Erik", TEST_DATE.minusSeconds(5));
    given(theBarrista.parse(message1.get())).willReturn(chatCommand1);
    given(theBarrista.parse(message2.get())).willReturn(chatCommand2);
    given(theBarrista.parse(message3.get())).willReturn(chatCommand3);
    given(theBarrista.parse(follow.get())).willReturn(followCommand);
    given(theBarrista.parse(wall.get())).willReturn(wallCommand);
    given(waiter.getCommandHistory())
        .willReturn(ImmutableList.of(chatCommand1, chatCommand2, chatCommand3, followCommand, wallCommand));
    given(timeService.now()).willReturn(TEST_DATE);

    controller.startAcceptingInput();

    verify(waiter, times(6)).addCommand(any(HipsterCommand.class));
    verify(chat).writeToTerminal(String.format("%s (%s)\n", message2.get(), "1 minutes ago"));
    verify(chat).writeToTerminal(String.format("%s (%s)\n", message1.get(), "5 minutes ago"));
  }

  @Test
  public void shouldBePossibleToExitTheChat() {
    Optional<String> exit = COMMAND_STRING_EXIT;
    given(chat.nextCommand()).willReturn(exit);
    GoodbyeCommand goodbyeCommand = new GoodbyeCommand(TEST_DATE);
    given(theBarrista.parse(COMMAND_STRING_EXIT.get())).willReturn(goodbyeCommand);

    controller.startAcceptingInput();

    verify(waiter).addCommand(goodbyeCommand);
    verify(chat).writeToTerminal("Goodbye!");
    verify(janitor).shutdown();
  }
}

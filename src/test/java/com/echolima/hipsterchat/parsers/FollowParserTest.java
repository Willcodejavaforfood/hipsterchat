package com.echolima.hipsterchat.parsers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.FollowCommand;
import com.echolima.hipsterchat.parsers.FollowParser;

@RunWith(MockitoJUnitRunner.class)
public class FollowParserTest {
  private static final LocalDateTime TEST_TIME = LocalDateTime.of(2015, 1, 1, 1, 1);
  @InjectMocks
  private FollowParser parser;
  @Mock TimeService timeService;

  @Test
  public void shouldRecogniseAFollowCommand() {
    assertThat("Did not recognise a follow command", parser.canHandle("A follows B"), is(true));
  }

  @Test
  public void shouldParseAFollowCommand() {
    given(timeService.now()).willReturn(TEST_TIME);
    FollowCommand followCommand = (FollowCommand) parser.parse("A follows B");
    assertThat("Did not set user correctly", followCommand.getUser(), is("A"));
    assertThat("Did not set following correctly", followCommand.getFollowing(), is("B"));
    assertThat("Did not set time correctly", followCommand.getDate(), is(TEST_TIME));
  }
}
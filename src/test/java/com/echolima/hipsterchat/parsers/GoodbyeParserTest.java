package com.echolima.hipsterchat.parsers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.GoodbyeCommand;
import com.echolima.hipsterchat.parsers.GoodbyeParser;
import junit.framework.TestCase;

@RunWith(MockitoJUnitRunner.class)
public class GoodbyeParserTest extends TestCase {
  private static final LocalDateTime TEST_TIME = LocalDateTime.of(2015, 1, 1, 1, 1);
  @InjectMocks
  private GoodbyeParser parser;
  @Mock TimeService timeService;

  @Test
  public void shouldRecogniseAGoodbyeCommand() {
    assertThat("Did not recognise a goodbye command", parser.canHandle("Exit"), is(true));
  }

  @Test
  public void shouldParseAGoodbyeCommand() {
    given(timeService.now()).willReturn(TEST_TIME);
    GoodbyeCommand goodbye = (GoodbyeCommand) parser.parse("Exit");
    assertThat("Did not set time correctly", goodbye.getDate(), is(TEST_TIME));
  }
}
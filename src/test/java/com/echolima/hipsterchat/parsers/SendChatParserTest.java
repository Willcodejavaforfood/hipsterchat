package com.echolima.hipsterchat.parsers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.SendChatCommand;
import com.echolima.hipsterchat.parsers.SendChatParser;

@RunWith(MockitoJUnitRunner.class)
public class SendChatParserTest {
  private static final LocalDateTime TEST_TIME = LocalDateTime.of(2015, 1, 1, 1, 1);
  @InjectMocks
  private SendChatParser parser;
  @Mock TimeService timeService;

  @Test
  public void shouldRecogniseASendChatCommand() {
    assertThat("Did not recognise a send chat command", parser.canHandle("A -> hello"), is(true));
  }

  @Test
  public void shouldParseASendChatCommand() {
    given(timeService.now()).willReturn(TEST_TIME);
    SendChatCommand followCommand = (SendChatCommand) parser.parse("A -> hello");
    assertThat("Did not set user correctly", followCommand.getUser(), is("A"));
    assertThat("Did not set message correctly", followCommand.getMessage(), is("hello"));
    assertThat("Did not set time correctly", followCommand.getDate(), is(TEST_TIME));
  }
}
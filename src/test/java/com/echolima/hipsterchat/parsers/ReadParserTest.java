package com.echolima.hipsterchat.parsers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.ReadCommand;
import com.echolima.hipsterchat.parsers.ReadParser;

@RunWith(MockitoJUnitRunner.class)
public class ReadParserTest {
  private static final LocalDateTime TEST_TIME = LocalDateTime.of(2015, 1, 1, 1, 1);
  @InjectMocks
  private ReadParser parser;
  @Mock TimeService timeService;

  @Test
  public void shouldRecogniseAReadCommand() {
    assertThat("Did not recognise a read command", parser.canHandle("A"), is(true));
  }

  @Test
  public void shouldParseAReadCommand() {
    given(timeService.now()).willReturn(TEST_TIME);
    ReadCommand readCommand = (ReadCommand) parser.parse("A");
    assertThat("Did not set user correctly", readCommand.getUser(), is("A"));
    assertThat("Did not set time correctly", readCommand.getDate(), is(TEST_TIME));
  }
}
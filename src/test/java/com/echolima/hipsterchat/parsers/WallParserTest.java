package com.echolima.hipsterchat.parsers;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;

import com.echolima.hipsterchat.TimeService;
import com.echolima.hipsterchat.commands.WallCommand;
import com.echolima.hipsterchat.parsers.WallParser;

@RunWith(MockitoJUnitRunner.class)
public class WallParserTest {
  private static final LocalDateTime TEST_TIME = LocalDateTime.of(2015, 1, 1, 1, 1);
  @InjectMocks
  private WallParser parser;
  @Mock TimeService timeService;

  @Test
  public void shouldRecogniseAWallCommand() {
    assertThat("Did not recognise a wall command", parser.canHandle("A wall"), is(true));
  }

  @Test
  public void shouldParseAWallCommand() {
    given(timeService.now()).willReturn(TEST_TIME);
    WallCommand wallCommand = (WallCommand) parser.parse("A wall");
    assertThat("Did not set user correctly", wallCommand.getUser(), is("A"));
    assertThat("Did not set time correctly", wallCommand.getDate(), is(TEST_TIME));
  }
}